# Use an official Python runtime as a parent image
FROM python:3.8-slim-buster

# Set the working directory in the container
WORKDIR /usr/src/app

# Install Ansible via pip
RUN pip install --no-cache-dir ansible

# Copy the current directory contents into the container at /usr/src/app
COPY . .

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME Ansible

# Run ansible-playbook when the container launches
CMD ["ansible-playbook", "--version"]
